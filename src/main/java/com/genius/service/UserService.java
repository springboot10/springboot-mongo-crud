package com.genius.service;

import com.genius.entity.User;
import java.util.List;

public interface UserService {

    public List<User> getUserAllService();

    public Object getUserByIdService(String key);

    public User saveUserService(User payload);

    public Object updateUserByIdService(String key, User payload);

    public String deleteUserByIdService(String key);
}