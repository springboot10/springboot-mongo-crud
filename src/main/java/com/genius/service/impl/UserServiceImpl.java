package com.genius.service.impl;

import com.genius.entity.User;
import com.genius.repository.UserRepository;
import com.genius.service.UserService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getUserAllService() {
        return userRepository.findAll();
    }

    @Override
    public Object getUserByIdService(String key) {
        var objectId = new ObjectId(key);
        var userOptional = userRepository.findById(objectId);
        if (userOptional.isPresent()) {
            return userOptional.get();
        } else {
            return "Data not found";
        }
    }

    @Override
    public User saveUserService(User payload) {
        return userRepository.save(payload);
    }

    @Override
    public Object updateUserByIdService(String key, User payload) {
        var objectId = new ObjectId(key);
        var userOption = userRepository.findById(objectId);
        if (userOption.isPresent()) {
            var user = userOption.get();
            if (payload.getName() != null && !payload.getName().isBlank()) {
                user.setName(payload.getName());
            }
            if (payload.getAge() != null) {
                user.setAge(payload.getAge());
            }
            if (payload.getSalary() != null) {
                user.setSalary(payload.getSalary());
            }
            if (payload.getQuotation() != null && !payload.getQuotation().isBlank()) {
                user.setQuotation(payload.getQuotation());
            }
            if (payload.getPosition().size() != 0) {
                user.setPosition(payload.getPosition());
            }
            var userResponse = userRepository.save(user);
            return userResponse;
        } else {
            return "Data not found";
        }
    }

    @Override
    public String deleteUserByIdService(String key) {
        var objectId = new ObjectId(key);
        var userOptional = userRepository.findById(objectId);
        if (userOptional.isPresent()) {
            userRepository.deleteById(objectId);
            return "Delete key " + key + " is success";
        } else {
            return "Data not found";
        }
    }
}


