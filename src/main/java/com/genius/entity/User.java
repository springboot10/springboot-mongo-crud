package com.genius.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("users")
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {
    @Id
    private ObjectId _id;
    private String name;
    private Integer age;
    private List<String> position;
    private Integer salary;
    private String quotation;
}
