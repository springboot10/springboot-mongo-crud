package com.genius.controller;

import com.genius.entity.User;
import com.genius.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping()
    public List<User> getUserAll() {
        return userService.getUserAllService();
    }

    @GetMapping("/{key}")
    public Object getUserById(@PathVariable("key") String key) {
        return userService.getUserByIdService(key);
    }

    @PostMapping()
    public User saveUser(@RequestBody User payload){
        return userService.saveUserService(payload);
    }

    @PutMapping("/{key}")
    public Object updateUserById(@PathVariable("key") String key, @RequestBody User payload){
        return userService.updateUserByIdService(key, payload);
    }

    @DeleteMapping("/{key}")
    public String deleteUserById(@PathVariable("key")String key){
        return userService.deleteUserByIdService(key);
    }

}
